FROM ruby:2.7.4

WORKDIR /opt/horobot2
COPY bin bin
COPY lib lib
COPY Gemfile Gemfile.lock .

VOLUME /opt/horobot2/var

RUN \
  bundle config set --local path 'vendor/bundle'; \
  bundle config set no-cache 'true'; \
  bundle install;

CMD ["/opt/horobot2/bin/horobot2"]
