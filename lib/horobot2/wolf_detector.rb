# frozen_string_literal: true

require 'classifier-reborn'

##
# WolfDetector detects if a user is a wolf.

class HoroBot2::WolfDetector


  attr_reader :bot


  def initialize(bot, detector_config = {})
    @bot = bot
    @trained_data_filename = detector_config[:trained_data_filename] || raise(ArgumentError, 'No trained data is given to WolfDetector')
    @record_count_max = detector_config[:record_count_max] || 100
    @record_count_min = detector_config[:record_count_min] || 10

    @classifier = Marshal.load(File.read(@trained_data_filename))

    @user_past_classifications_hash = {} # { String => Array<Boolean> }
  end


  ##
  # This method is called when received a message.

  def receive(message)
    if message.author && message.text && !message.text.strip.empty?
      past_classifications = @user_past_classifications_hash[message.author] ||= []
      classification = @classifier.classify(message.text) == 'Wolf'
      past_classifications << classification
      while past_classifications.length > @record_count_max
        past_classifications.shift
      end
      @bot.logger.debug('WolfDetector') { "'#{message.author}' has been classified as #{classification ? '' : 'not '}wolf." }
    end
  end


  ##
  # Get the result of wolf detection.

  def detect_user(user)
    past_classifications = @user_past_classifications_hash[user]
    if past_classifications && past_classifications.length >= @record_count_min
      "#{user} 有 #{(past_classifications.count(true).to_f / past_classifications.length.to_f * 100).round}% 的概率是咱的同类呐。"
    else
      "没有足够的聊天记录，即使是咱也无法辨认 #{user} 是不是咱的同类呐。"
    end
  end


  def to_hash
    {
      trained_data_filename: @trained_data_filename,
      record_count_max: @record_count_max,
      record_count_min: @record_count_min,
    }
  end

  alias_method :to_h, :to_hash


  module Tokenizer

    ##
    # Split string into 2-character and Latin word array.

    def self.call(str)
      str.scan(/[\u002f-\u024f]+|./).map(&:strip).reject(&:empty?).reduce([]) do |memo, word|
        last_word = memo.last
        if last_word.nil? || last_word.length >= 4 || last_word + word =~ /[\u002f-\u024f]/
          memo + [word]
        else
          memo[0..-2] + [last_word + word]
        end
      end.map {|word| ClassifierReborn::Tokenizer::Token.new(word) }
    end

  end


end
