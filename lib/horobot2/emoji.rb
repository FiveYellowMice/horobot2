##
# An Emoji represents an Emoji defined in Unicode tr51.

class HoroBot2::Emoji < String

  module EmojiRegex

    ranges = '(?:' + [
      "[\u2700-\u27bf]\ufe0f?", # Dingbats, optionally attached by a VARIATION SELECTOR-16
      "[\u{1f300}-\u{1f3fa}]",  # Miscellaneous Symbols and Pictographs
      "[\u{1f400}-\u{1f64f}]",  # Miscellaneous Symbols and Pictographs + Emoticons
      "[\u{1f680}-\u{1f6ff}]",  # Transport and Map Symbols
      "[\u{1f900}-\u{1f9ff}]",  # Supplemental Symbols and Pictographs
      "[\u2460-\u26ff]\ufe0f",  # Enclosed Alphanumerics + Box Drawing + Block Elements + Geometric Shapes + Miscellaneous Symbols, attached by a VARIATION SELECTOR-16
    ].join('|') + ')'

    modifier = "[\u{1f3fb}-\u{1f3ff}]" # Skin tones

    flags = "(?:[\u{1f1e6}-\u{1f1ff}]){2}"

    joiner = "\u200d"

    modifier_sequence = "#{ranges}(?:#{modifier})?"

    zwj_sequence = "(?:#{modifier_sequence}#{joiner})*#{modifier_sequence}"

    final = [
      flags,
      zwj_sequence
    ].join('|')

    ONE = Regexp.new("^(?:#{final})$")
    MANY = Regexp.new(final)
    SEQUENCE = Regexp.new("^(?:#{final})+$")
    SEQUENCES = Regexp.new("(?:#{final})+")
    SEQUENCE_OF_SAME = Regexp.new("^(#{final})\\1*$")

    # References:
    # http://crocodillon.com/blog/parsing-emoji-unicode-in-javascript
    # http://www.unicode.org/Public/emoji/3.0//emoji-data.txt
    # http://unicode.org/Public/emoji/3.0/emoji-sequences.txt
    # http://www.unicode.org/Public/emoji/3.0//emoji-zwj-sequences.txt
    # http://ftp.unicode.org/Public/UNIDATA/Blocks.txt

  end

  def initialize(*args)
    raise(HoroBot2::EmojiError, "汝不想被刷屏吧？") if args[0].length > 64
    raise(HoroBot2::EmojiError, "咱不觉得 '#{args[0]}' 是个 Emoji 。") unless args[0] =~ EmojiRegex::SEQUENCE
    super(*args)
  end

  def valid?
    self =~ EmojiRegex::SEQUENCE
  end

  def single?
    self =~ EmojiRegex::ONE
  end

  def sequence_of_same?
    self =~ EmojiRegex::SEQUENCE_OF_SAME
  end

  def to_single_emoji
    if single?
      self
    else
      EmojiRegex::MANY.match(self)[0]
    end
  end

end
